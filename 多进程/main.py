# coding:utf-8
from multiprocessing import Pool
from redlock import Redlock

dlm = Redlock([{"host": "127.0.0.1", "port": 6379, "db": 0}], retry_count=3, retry_delay=0.2)


def test_pool():
    my_lock = dlm.lock("my_resource_name", 1000)
    print(my_lock)
    a = dlm.unlock(my_lock)
    print(a)


if __name__ == '__main__':

    pool = Pool(2)
    for i in range(0, 5):
        pool.apply_async(test_pool)
    pool.close()
    pool.join()
