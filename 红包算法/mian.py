# coding:utf-8
import random


def redPacket(people, money):
    result = []
    remain = people
    max_money = money / people * 2
    for i in range(people):
        remain -= 1
        if remain > 0:
            m = random.randint(1, min(money - remain, max_money))
        else:
            m = money
        result.append(m / 100.0)
        money -= m
    return result


data1 = range(1, 10)
data = list()

for i in range(1, 20):
    # print(redPacket(10, 10000))
    for me in redPacket(10, 10000):
        num = 1
        a = {
            "num": num,
            "name": "hongbao{}".format(i),
            "type": "",
            "me": me,
        }
        num += 1
        data.append(a)
# print(data)
# print(data1)
