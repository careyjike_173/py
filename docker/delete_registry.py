# coding:utf-8
import requests
import sys
import json

host = ''
port = 5000
https = True

if len(sys.argv) < 2:
    print("参数错误: None")
    sys.exit(1)


def get_images_names(repo_ip, repo_port):
    docker_images = []
    if https:
        url = "https://" + repo_ip + "/v2/_catalog"
    else:
        url = "http://" + repo_ip + ":" + str(repo_port) + "/v2/_catalog"
    res = requests.get(url).content.strip()
    res_dic = json.loads(res)
    images_type = res_dic['repositories']
    for i in images_type:
        if https:
            url2 = "https://" + repo_ip + "/v2/" + str(i) + "/tags/list"
        else:
            url2 = "http://" + repo_ip + ":" + str(repo_port) + "/v2/" + str(i) + "/tags/list"
        res2 = requests.get(url2).content.strip()
        res_dic2 = json.loads(res2)
        name = res_dic2['name']
        tags = res_dic2['tags']
        if not isinstance(tags, list):
            continue
        for tag in tags:
            if https:
                docker_name = str(repo_ip) + "/" + name + ":" + tag
            else:
                docker_name = str(repo_ip) + ":" + str(repo_port) + "/" + name + ":" + tag
            docker_images.append(docker_name)
            # print docker_name
    return docker_images


images_list = get_images_names(repo_ip=host, repo_port=port)
delete_images_list = sys.argv[1:]

for image in delete_images_list:
    if image not in images_list:
        print("{}: image not found".format(image))
        continue

    imgs = image.split('/')[-1]
    img, ver = imgs.split(':')[0], imgs.split(':')[1]
    headers = {
        "Accept": "application/vnd.docker.distribution.manifest.v2+json"
    }
    if https:
        etag_url = "https://{}/v2/{}/manifests/{}".format(host, img, ver)
    else:
        etag_url = "http://{}:{}/v2/{}/manifests/{}".format(host, port, img, ver)
    ret = requests.get(url=etag_url, headers=headers)
    if ret.status_code != requests.codes.ok:
        print("{}:{} 删除失败".format(img, ver))
        continue

    etag = ret.headers.get("Etag").replace("\"", "")
    if https:
        delete_images_url = "https://{}/v2/{}/manifests/{}".format(host, img, etag)
    else:
        delete_images_url = "http://{}:{}/v2/{}/manifests/{}".format(host, port, img, etag)
    del_ret = requests.delete(url=delete_images_url)
    if del_ret.ok:
        print("{}:{} 删除成功".format(img, ver))
    else:
        print("{}:{} 删除失败".format(img, ver))
