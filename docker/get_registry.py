# coding:utf-8

import requests
import json

repo_ip = ''
repo_port = 5000
https = True


def get_images_names(repo_ip, repo_port):
    docker_images = []
    if https:
        url = "https://" + repo_ip + "/v2/_catalog"
    else:
        url = "http://" + repo_ip + ":" + str(repo_port) + "/v2/_catalog"
    res = requests.get(url).content.strip()
    res_dic = json.loads(res)
    images_type = res_dic['repositories']
    for i in images_type:
        if https:
            url2 = "https://" + repo_ip + "/v2/" + str(i) + "/tags/list"
        else:
            url2 = "http://" + repo_ip + ":" + str(repo_port) + "/v2/" + str(i) + "/tags/list"
        res2 = requests.get(url2).content.strip()
        res_dic2 = json.loads(res2)
        name = res_dic2['name']
        tags = res_dic2['tags']
        if not isinstance(tags, list):
            continue
        for tag in tags:
            if https:
                docker_name = str(repo_ip) + "/" + name + ":" + tag
            else:
                docker_name = str(repo_ip) + ":" + str(repo_port) + "/" + name + ":" + tag
            docker_images.append(docker_name)
            print docker_name
    return docker_images


a = get_images_names(repo_ip, repo_port)
print(a)
